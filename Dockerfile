ARG PYVERSION=3.9
FROM python:${PYVERSION}

RUN apt update && apt install git -y
RUN pip install -U pip python-semantic-release
