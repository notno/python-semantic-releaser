# python semantic releaser

* Docker + python + [python-semantic-release](https://python-semantic-release.readthedocs.io/en/latest/). Made with love for Gitlab CI
* `python-semantic-release` will try to build your package to release it. We provide `3.6`, `3.7`, `3.8` and `3.9` python images. Each is updated to the latest every night. The `latest` image points to the latest `3.9` tag. Pick your tag accordingly.
